<h1>Jughead Music</h1>

<p>Jughead Music is a project created for educational purposes (managing Databases, HTML and PHP) internally. It was worked with Sebastian Riquelme and Christian Seguel.</p>

<br >

<h2>WARNING!</h2>
<p>This website was made for educational purposes. The material used is copyrighted and was purchased in full (songs purchased and/or authorized by the bands or soloists of the groups).
This code may not be reused for educational, advertising or experimental purposes.</p>

<br >

<p>NEVER STOP PROGRAMMING.</p>
<p>Luciano Sánchez</p>

<img src="https://loverandom.cl/wp-content/uploads/2018/12/logo-lrcl-dark.png">
